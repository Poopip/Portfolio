<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CDN BOOTSTRAP -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- CSS -->
  <link href="css/css.css" rel="stylesheet">
  <!-- GOOGLE FONT RALEWAY -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,900" rel="stylesheet">
  <!-- CDN FONT AWSOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <title>Accueil</title>
</head>

<body id="body">

  <!-- Navigation
  ================================================== -->
  <header>
    <div class="container-fluid">
      <div class="row">
        <nav class="navbar">
          <div class="navbar-header text-center col-12 col-sm-12 col-md-12">
            <a class="navbar-brand" href="#page-top"><img class="img-fluid" src="images/logo-header.png" alt="logo-header"></a>
          </div>
          <button class="col-2 navbar-toggler fas fa-bars fa-2x" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"></button>
          <button class="customBtn greyBtn">My Urban Soccer<p class="subTitle">Mon compte</p></button>
          <button class="customBtn orangeBtn">Inscription</button>
          
          <div class="collapse navbar-collapse" id="navbarSupportedContent1">  
            <ul class="navbar-nav">
              <li class="hidden"><a class=nav-link href="#page-top"></a></li>
              <li class="nav-item"><a class=nav-link href="#">SELECTIONNEZ VOTRE CENTRE</a></li>
              <li class="nav-item"><a class=nav-link href="#">LEAGUES</a></li>
              <li class="nav-item"><a class=nav-link href="#">KIDS</a></li>
              <li class="nav-item"><a class=nav-link href="#">CUP/TOURNOIS</a></li>
              <li class="nav-item"><a class=nav-link href="#">EVENTS</a></li>
              <li class="nav-item"><a class=nav-link href="#">STORE</a></li>
              <li class="nav-item"><a class=nav-link href="#">VIDEOS</a></li>
              <li class="nav-item"><a class=nav-link href="#">URBANSOCCER</a></li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <!-- Caroussel
  ================================================== -->
  <div class="container-fluid carousel">
    <div class="row">
      <div class="carousel">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <!-- INDICATEURS DE SLIDE DU CAROUSEL -->
          <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>           
          </ol>
          <!-- CONTENU DU CAROUSEL -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="img-fluid" src="images/slide-01.jpg" alt="slide1">
            </div>
            <div class="carousel-item">
              <img class="img-fluid" src="images/slide-02.jpg" alt="slide2">
            </div>
          </div>
          <!-- BOUTONS DE DEFILEMENT DU CAROUSEL -->
          <a class="carousel-control-prev" href="#carousel" data-slide="prev">
            <span class="fas fa-chevron-left"></span>
          </a>
          <a class="carousel-control-next" href="#carousel" data-slide="next">
              <span class="fas fa-chevron-right"></span>
          </a>
        </div>
      </div>
    </div>
    </div>

  <!-- Corps
  ================================================== -->
  <section>

    <!-- Encarts -->
    <div class="container">
      <div data-toggle="collapse" href="#nosCentres" aria-expanded="false" aria-controls="nosCentres">
        <div class="header encartTitle blackTitle">
          <h5>NOS CENTRES</h5>
          <p class="subTitle">Trouvez votre centre</p>
        </div>
      </div>
      <div class="container" id="nosCentres">
        <div class="container">
          <img class="img-fluid" src="images/encart-01.jpg" alt="encart-01">
        </div>
        <div class="container">
          <button class="customBtn blackBtn"><span class="fas fa-chevron-right fa-sm chevronBtn"></span>CHERCHER</button>
        </div>
      </div>
    </div>

    <div class="container">
      <div data-toggle="collapse" href="#Actus" aria-expanded="false" aria-controls="Actus">
        <div class="header encartTitle orangeTitle">
          <h5>ACTUALITE</h5>
          <p class="subTitle">Toute l'actu de vos centres</p>
        </div>
      </div>
      <div class="container" id="Actus">
        <div class="container">
          <img class="img-fluid" src="images/encart-02.jpg" alt="encart-02">
        </div>
        <div class="container">
          <p class="content titleContent">Dapibus ut ac augue scelerisque</p>
          <p class="content txtContent">Dapibus ut ac augue scelerisque sed ac urna lorem et scelerisque, amet, rhoncus adipiscing turpis ...</p>
        </div>
        <div class="container">
          <button class="customBtn blackBtn"><span class="fas fa-chevron-right fa-sm chevronBtn"></span>TOUTE L'ACTU</button>
        </div>
      </div>
    </div>

    <div class="container">
      <div data-toggle="collapse" href="#Store" aria-expanded="false" aria-controls="Store">
        <div class="header encartTitle blackTitle">
          <h5>STORE</h5>
          <p class="subTitle">Equipement, flocage...</p>
        </div>
      </div>
      <div class="container" id="Store">
        <div class="container">
          <img class="img-fluid" src="images/encart-03.jpg" alt="encart-03">
        </div>
        <div class="container">
          <p class="content titleContent">Lorem ipsum dolor sit amet</p>
          <p class="content txtContent">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="container">
          <button class="customBtn blackBtn"><span class="fas fa-chevron-right fa-sm chevronBtn"></span>VOIR LE STORE</button>
        </div>
      </div>
    </div>

    <!-- Banners -->
    <img class="img-fluid" src="images/banner-01.jpg" alt="banner-01">
    <img class="img-fluid" src="images/banner-02.jpg" alt="banner-02">
    <img class="img-fluid" src="images/banner-03.jpg" alt="banner-03">
    <div class="text-center ban04">
      <img class="img-fluid" src="images/banner-04.jpg" alt="banner-04">
    </div>

    <!-- Partenaires -->
    <div class="container partenaires">
        <p>NOS PARTENAIRES</p>
        <img src="images/partenaire-01.png" alt="partenaire-01">
        <img src="images/partenaire-02.png" alt="partenaire-02">
        <img src="images/partenaire-03.png" alt="partenaire-03">
        <img src="images/partenaire-04.png" alt="partenaire-04">
        <img src="images/partenaire-05.png" alt="partenaire-05">
        <img src="images/partenaire-06.png" alt="partenaire-06">
    </div>
  </section>

  <!-- Footer
  ================================================== -->
  <footer>
    
    <!-- METTRE LES LOGOS RESEAUX SOCIAUX -->

    <div class="container footer">
      <div class="container text-center">
        <ul class="nav1-footer">
          <li class="nav-item"><a href="#">SELECTIONNEZ VOTRE CENTRE /</a></li>
          <li class="nav-item"><a href="#">LEAGUES /</a></li>
          <li class="nav-item"><a href="#">KIDS /</a></li>
          <li class="nav-item"><a href="#">CUP-TOURNOIS /</a></li>
          <li class="nav-item"><a href="#">EVENTS /</a></li>
          <li class="nav-item"><a href="#">STORE /</a></li>
          <li class="nav-item"><a href="#">VIDEOS /</a></li>
          <li class="nav-item"><a href="#">URBANSOCCER</a></li>
        </ul>
      </div>
      <div class="container text-center img-footer">
        <img src="images/logo-footer.png" alt="logo-footer">
      </div>
      <div class="container text-center">
        <ul class="nav2-footer">
          <li class="nav-item"><a href="#">CONDITIONS GENERALES</a></li>
          <li class="nav-item"><a href="#">| MENTIONS LEGALES</a></li>
          <li class="nav-item"><a href="#">| PLAN DU SITE</a></li>
          <li class="nav-item"><a href="#">| CONTACT</a></li>
        </ul>
      </div>
    </div>
  </footer>

  <!-- Scripts
  ================================================== -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <script>
    // Vitesse defilement carousel
    $(function () {
      $('.carousel').carousel({ interval: 4500 });
    });
    // Repli automatique du menu en mode smartphone
    $( function() {
      if (screen.width < 768) {
    $('.nav a').on('click', () => $('button.navbar-toggler').click());
    }});
    // Scrollspy fluide
    $(function () {
      $('header a').on('click', function(e) {
        e.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(this.hash).offset().top
        }, 500, function(){
          window.location.hash = hash;
        });
      });
    });
  </script>

</body>
</html>