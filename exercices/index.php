<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8" />
<title>Exercices</title>
</head>

<body>

    <!-- NAVIGATION PRINCIPAL -->
    <?php include("../includes/navigation_principale.php") ?>

    <!-- DESCRIPTION -->
    <?php include("../includes/descriptif.php") ?>

    <!-- FOOTER -->
    <?php include("../includes/footer.php"); ?>
    
</body>
</html>
