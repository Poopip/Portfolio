<html>
<head>
	<title>Accueil - Oiseau</title>
	<meta charset="UFT-8">
	<link rel="stylesheet" href="style.css">
</head>

<body>

	<!-- NAVIGATION PRINCIPAL -->
	<?php include("../../includes/navigation_principale.php") ?>

	<div id="bird">
		<div id="en_tete">
			<h1 class="h1">3 oiseaux du monde</h1>
			<ul id="ul">
				<li class="li"><a class="a" href="#acanthize_nain">acanthize nain</a></li>
				<li class="li"><a class="a" href="#gypaete_barbu">gypaète</a></li>
				<li class="li"><a class="a" href="#dacnis_coiffe_bleue">dacnis a coiffe bleue</a></li>
			</ul>
		</div>

		<div id="acanthize_nain">
			<h2 class="h2">L'Acanthize nain</h2>
			<img class="img" src="../../img/acanthize.nain.jpg" alt="acanthize_nain">
			<p class="p">L'Acanthize nain (<em>Acanthiza nana</em>) est une espèce de <a class="a" href="http://fr.wikipedia.org/wiki/Passereau" >passereau</a> que l'on rencontre en <a class="a" href="http://fr.wikipedia.org/wiki/Australie">Australie</a>. C'est une espèce protégée sous la convention National Parks and Wildlife Act, 1974.</p>
			<p class="p">C'est un petit oiseau à l'allure élégante. Il est discret, mais sait s'imposer. Il aime la proximité des autres oiseaux, il est rarement mis à l'écart de son groupe.</p>
			<a class="haut a" href="#">haut</a>
			<hr>
		</div>

		<div id="gypaete_barbu">
			<h2 class="h2">Le Gypaète barbu</h2>
			<img class="img" src="../../img/gypaete.barbu.jpg" alt="gypaète_barbu">
			<p class="p">Le Gypaète barbu (<em>Gypaetus barbatus</em>) est une des quatre grandes espèces de <a class="a" href="http://fr.wikipedia.org/wiki/Vautour">vautours</a> européens. C'est la seule espèce du genre Gypaetus. Il appartient à l'ordre des Accipitriformes et à la famille des Accipitridés.</p>
			<a class="haut a" href="#">haut</a>
			<hr>
		</div>

		<div id="dacnis_coiffe_bleue">
			<h2 class="h2">Le Dacnis a coiffe bleue</h2>
			<img class="img" src="../../img/dacnis.a.coiffe.bleue.jpg" alt="dacnis_a_coiffe_bleue">
			<p class="p">Le Dacnis à coiffe bleue (<em>Dacnis lineata</em>) est une espèce de <a class="a" href="http://fr.wikipedia.org/wiki/Passereau">passereau</a> de la famille des Thraupidae.
			On le trouve dans les forêts humides du <a class="a" href="http://fr.wikipedia.org/wiki/Bassin_de_l%27Amazone">bassin de l'Amazone</a>.</p>
			<a class="haut a" href="#">haut</a>
			<hr>
		</div>
	</div>

	<!-- FOOTER -->
	<?php include("../../includes/footer.php"); ?>

</body>

</html>
