<?php

/////////////////////////////////////////////
// DEBUT CLASSE Personnage

abstract class Personnage 
{
    // DECLARATION DES ATTRIBUTS
    protected $id,
              $nom,
              $degats,
              $timeEndormi,
              $type,
              $atout;

    // DECLARATION DES CONSTANTES
    const CEST_MOI = 1; // Constante renvoyée par frapper() si on se frappe soi-même
    const PERSONNAGE_TUE = 2; // Cosntante renvoyée par frapper() si on tue le personnage en le frappant
    const PERSONNAGE_FRAPPE = 3; // Constante renvoyé frapper() si on a bien frappé le personnage
    const PERSONNAGE_ENSORCELE = 4; // Constante renvoyée par lancerUnSort() dans classe Magicien si on a bien ensorcelé un personnage
    const PAS_DE_MAGIE = 5; // Constante renvoyée par lancerUnSort() dans classe Magicien si on veut jeter un sort alors que la magie du magicien est à 0
    const PERSO_ENDORMI = 6; // Constante renvoyée par frapper() si le personnage qui veut frapper est endormi

    // CONSTRUCTEUR
    public function __construct(array $donnees) 
    {
        $this->hydrate($donnees);
        $this->type = strtolower(static::class); // On oblige le type a être en minuscule
    }

    // DECLARATION DES FONCTIONNALITES
    public function estEndormi() 
    {
        // On indique pendant combien de temps le personnage dort encore
        return $this->timeEndormi > time();
    }

    public function frapper(Personnage $perso) 
    { 
        
        // Vérifier qu'on ne se frappe pas soi-même
        if ($perso->id() == $this->id) 
        {
            return self::CEST_MOI;
        }

        // On vérifie que le personnage qui frappe n'est pas endormi
        elseif ($this->estEndormi()) 
        {
            return self::PERSO_ENDORMI;
        }

        // on indique au perso  qu'il doit recevoir des degats
        // puis on retourne la valeur renvoyée par la méthode : self::PERSONNAGE_TUE ou self::PERSONNAGE_FRAPPE
        return $perso->recevoirDegats();
    }

    // HYDRATATION
    public function hydrate(array $donnees) 
    {
        foreach ($donnees as $key => $value) 
        {
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method)) 
            {
                $this->$method($value);
            }
        }
    }

    public function nomValide() {
        return !empty($this->nom);
    }

    public function recevoirDegats() {
        // On augmente de 5 les dégâts
        $this->degats += 5; 

        // Si on a 100 ou plus de dégâts on supprime le personnage de la BDD
        if ($this->degats >= 100) {
            return self::PERSONNAGE_TUE;
        }
        // Sinon, on met a jour les dégâts du personnage
        return self::PERSONNAGE_FRAPPE;
    }

    // Fonction reveil() pour obtenir la date de reveil du personnage sous le format HH,mm,ss qui s'affichera dans le cadre d'information du personnage
    public function reveil() 
    {
        $secondes = $this->timeEndormi;
        $secondes -= time();

        $heures = floor($secondes / 3600);
        $secondes -= $heures * 3600;
        $minutes = floor($secondes / 60);
        $secondes -= $minutes * 60;

        $heures .= $heures <= 1 ? ' heure' : ' heures';
        $minutes .= $minutes <= 1 ? ' minute' : ' minutes';
        $secondes .= $secondes <= 1 ? ' seconde' : ' secondes';

        return $heures . ', ' . $minutes . ' et ' . $secondes;
    }

    // DECLARATION GETTERS (pour pouvoir lire les attributs)
    
    public function atout() 
    {
        return $this->atout;
    }
    
    public function degats() 
    {
        return $this->degats;
    }

    public function id() 
    {
        return $this->id;
    }

    public function nom() 
    {
        return $this->nom;
    }

    public function timeEndormi() 
    {
        return $this->timeEndormi;
    }

    public function type() 
    {
        return $this->type;
    }
    
    // DECLARATION SETTERS (pour pouvoir modifier les valeurs des attributs)
    public function setAtout($atout) 
    {
        $atout = (int) $atout;
        if ($atout >= 0 && $atout <= 100)
        {
            $this->atout = $atout;
        }
    }

    public function setDegats($degats) 
    {
        $degats = (int) $degats;
        if ($degats >= 0 && $degats <= 100) 
        {
            $this->degats = $degats;
        }
    }

    public function setId($id) 
    {
        $id = (int) $id;
        if ($id > 0) 
        {
            $this->id = $id;
        }
    }

    public function setNom($nom) 
    {
        if (is_string($nom)) 
        {
            $this->nom = $nom;
        }
    }

    public function setTimeEndormi($time) 
    {
        $this->timeEndormi = (int) $time;
    }
}

// FIN CLASSE Personnage
/////////////////////////////////////////////


?>