<?php

class Database {

  private $db;

    // constructeur de connexion à la bdd
  public function __construct($login, $password, $database_name, $host) {
    $this->db = new PDO("mysql:dbname=$database_name;host=$host;charset=utf8",$login,$password);
    $this->db->setattribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db->setattribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
  }

  public function query($query, $params = false) {
    if($params) {
      $req = $this->db->prepare($query);
      $req->execute($params);
    }
    else {
      $req = $this->db->query($query);
    }
    return $req;
  }

  public function lastInsertId() {
    return $this->db->lastInsertId();
  }

}


?>