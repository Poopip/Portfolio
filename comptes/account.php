<?php 
require 'bootstrap.php';

App::getAuth()->restrict();

if(!empty($_POST)) {
  if(empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']) {
    $_SESSION['flash']['danger'] = "Les mots de passes ne correspondent pas";
  }
  else {
    $user_id = $_SESSION['auth']->id;
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    require_once 'bdd.php';
    $req = $db->prepare('UPDATE users SET password = ? WHERE id = ?')->execute([$password, $user_id]);
    $_SESSION['flash']['success'] = "Votre mot de passe a bien été mis a jour";
  }
}

// MENU PRINCIPAL
include("../includes/navigation_principale.php");
// HEADER REGISTER
include("header_register.php");
?>

<div class="container">

  <h1>Bonjour <?= $_SESSION['auth']->username; ?></h1>

  <form action="" method="post">
    <div class="form-group">
      <input class="form-control" type="password" name="password" placeholder="Changer de mot de passe">
    </div>
    <div class="form-group">
      <input class="form-control" type="password" name="password confirm" placeholder="Confirmation du mot de passe">
    </div>
    <button class="btn btn-primary">Changer mon mot de passe</button>
  </form>

</div>

<?php
// FOOTER
include('../includes/footer.php');
?>