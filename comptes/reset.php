<?php
require_once 'bootstrap.php';

if(isset($_GET['id']) && isset($_GET['token'])) {
  $auth = App::getAuth();
  $db = App::getDatabase();
  $user = $auth->checkResetToken($db, $_GET['id'], $_GET['token']);
  if($user) {
      if(!empty($_POST)) {
          $validator = new Validator($_POST);
          $validator->isConfirmed('password');
          if($validator->isValid()) {
              $password = $auth->hashPassword($_POST['password']);
              $db->query('UPDATE users SET password = ?, reset_at = NULL, reset_token = NULL WHERE id = ?', [$password, $_GET['id']]);
              $auth->connect($user);
              Session::getInstance()->setFlash('success','Votre mot de passe a bien été modifié');
              App::redirect('account.php');
          }
      }
  }else {
      Session::getInstance()->setFlash('danger',"Ce token n'est pas valide");
      App::redirect('login.php');
  }
}else {
  App::redirect('login.php');
}

// MENU PRINCIPAL
include("../includes/navigation_principale.php");
// HEADER REGISTER
include("header_register.php");

?>

<div id="main">
  <div class="container">

  <h1 class="text-center">Réinitialiser mon mot de passe</h1>

  <?php if(!empty($errors)): ?>
  <div class="alert alert-danger">
    <p>Vous n'avez pas rempli le formulaire correctement</p>

    <ul>
      <?php foreach($errors as $error): ?>
        <li><?= $error; ?></li>
      <?php endforeach; ?>
    </ul>
  </div>
  <?php endif; ?>

  <form action="" method="post">

    <div class="form-group">
      <label for="">Mot de Passe</label>
      <input type="password" name="password" class="form-control">
    </div>

    <div class="form-group">
      <label for="">Confirmation du mot de passe</label>
      <input type="password" name="password_confirm" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Réinitialiser votre mot de passe</button>

  </form>
  </div>
</div>

<?php 
// FOOTER
include('../includes/footer.php');
?>