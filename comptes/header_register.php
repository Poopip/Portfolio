<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">INSCRIPTION</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <?php if (isset($_SESSION['auth'])): ?>
          <li><a href="logout.php">Se déconnecter</a></li>
        <?php else: ?>
          <li><a href="register.php">S'inscrire</a></li>
          <li><a href="login.php">Se connecter</a></li>
        <?php endif; ?>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>

<?php if(Session::getInstance()->hasFlashes()): ?>
  <?php foreach(Session::getInstance()->getFlashes() as $type => $message): ?>
    <div class="alert alert-<?= $type; ?>">
      <?= $message; ?>

    </div>
  <?php endforeach; ?>
<?php endif; ?>
