<?php

require 'bootstrap.php';

if(!empty($_POST) && !empty($_POST['email'])){
  $db = App::getDatabase();
  $auth = App::getAuth();
  $session = Session::getInstance();
  if($auth->resetPassword($db, $_POST['email'])) {
      $session->setFlash('success', 'Les instructions du rappel de mot de passe vous ont été envoyées par email');
      App::redirect('login.php');
  } else {
      $session->setFlash('danger', 'Aucun compte ne correspond à cet adresse');
  }
}

// MENU PRINCIPAL
include("../includes/navigation_principale.php");
// HEADER REGISTER
include("header_register.php");

?>

<div id="main">
  <div class="container">

    <h1 class="text-center">Mot de passe oublié</h1>

    <form action="" method="post">

      <div class="form-group">
        <label for="">Email</label>
        <input type="email" name="email" class="form-control">
      </div>

      <button type="submit" class="btn btn-primary">Changer mon mot de passe</button>

    </form>
  </div>
</div>

<?php 
// FOOTER
include('../includes/footer.php');
?>