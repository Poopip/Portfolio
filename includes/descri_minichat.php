<div id="describe">
    <div class="container" >
    <p class="text-center">
      <a class="btn btn-custom btn-sm" data-toggle="collapse" href="#descriptionContent" role="button" aria-expanded="false" aria-controls="descriptionContent">Résumé</a>
    </p>
    <div class="row">
      <div class="col">
        <div class="collapse multi-collapse" id="descriptionContent">
          <p>Le mini-tchat est le résultat d'un exercice OpenClassrooms. Il m'as permis de faire mes tous premiers pas en PHP. J'ai dû apprendre à utiliser entre autres :</p>
          <ul class="list-group">
            <li class="list-group-item">- les fonction d'envois de formulaires</li>
            <li class="list-group-item">- les connexions à la BDD avec MySQL</li>
            <li class="list-group-item">- la sécurisation de base contre les injections de code</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

