<div id="describe">
    <div class="container" >
    <p class="text-center">
      <a class="btn btn-custom btn-sm" data-toggle="collapse" href="#descriptionContent" role="button" aria-expanded="false" aria-controls="descriptionContent">Résumé</a>
    </p>
    <div class="row">
      <div class="col">
        <div class="collapse multi-collapse" id="descriptionContent">
          <p>L'annuaire d'huiles essentielles est mon premier projet personnel. J'ai du utiliser :</p>
          <ul class="list-group">
            <li class="list-group-item">- les connexions BDD et les requêtes MySQL </li>
            <li class="list-group-item">- faire un système d'indexation en PHP et JQuery</li>
            <li class="list-group-item">- apprendre a utiliser les requêtes AJAX</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>