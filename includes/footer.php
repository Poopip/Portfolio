<!-- Footer -->
<footer class="footer navbar-fixed-bottom page-footer font-small special-color-dark">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
      <a href="http://adrienbenion.com"> Adrien BENION</a>
    </div>
    <!-- Copyright -->

</footer>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- Javascript de Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Footer -->