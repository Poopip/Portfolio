    <link rel="stylesheet" href="http://adrienbenion.com/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://adrienbenion.com/css/style.css">


    <header>
      <div class="container-fluid">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
          <div class="col-sm-5 navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a id="navTitle" class="navbar-brand" href="http://adrienbenion.com/index.php">Adrien BENION</a>
          </div>
          <div class="col-sm-7 collapse navbar-collapse" id="navbarSupportedContent">  
            <ul class="nav navbar-nav">
              <li class="hidden"><a href="#page-top"></a></li>
              <li>
                <a href="http://adrienbenion.com/jeux_combat/index.php">Jeux Combat</a>
              </li>
              <li>
                <a href="http://adrienbenion.com/huile_essentielles/index.php">Huiles Essentielles</a>
              </li>
              <li>
                <a href="http://adrienbenion.com/mini_chat/minichat.php">Mini Tchat</a>
              </li>
              <li class="dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="http://adrienbenion.com/exercices/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Exercices</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="http://adrienbenion.com/exercices/site_oiseaux/index.php">Page Oiseaux</a>
                </div>
              </li>
              <li>
                <a href="http://adrienbenion.com/trenta/index.php">Trenta</a>
              </li>
              <li>
                <a href="http://adrienbenion.com/comptes/register.php">Se connecter</a>
              </li>
            </ul>
          </div>
        </nav>    
      </div>
    </header>
