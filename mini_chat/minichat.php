<?php

?>

<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8" />
<title>Test minichat</title>
</head>

<body>

        <!-- MENU PRINCIPAL -->
        <?php include("../includes/navigation_principale.php") ?>

        <!-- DESCRIPTION -->
        <?php include("../includes/descri_minichat.php") ?>

      <div id="main">
        <form action="minichat_post.php" method="post">
            <p>
            <label for="pseudo">Pseudo</label> : <input type="text" name="pseudo" id="pseudo" value="<?php if(isset($_COOKIE['pseudo'])) {echo htmlspecialchars($_COOKIE['pseudo']);} ?>"/><br />
            <label for="message">Message</label> :  <input type="text" name="message" id="message" /><br />

            <input type="submit" value="Envoyer" />
            </p>
        </form>

        <div id="tchat">
            <?php

            // Je me connecte à la BDD
            include("../config.inc.php");

            try
            {
                $bdd = new PDO("mysql:host=$serveur;dbname=$nomBDD;charset=utf8",$login,$pass);
            }
            catch(Exception $e)
            {
                    die('Erreur : '.$e->getMessage());
            }

            // Je récupère les derniers messages
            $reponse = $bdd->query('SELECT pseudo, message, DATE_FORMAT(date_message, \'%d/%m/%Y %Hh%imin%ss\') AS date_message_fr FROM minichat ORDER BY date_message DESC LIMIT 0, 15');

            // J'affiche les messages
            while ($donnees = $reponse->fetch())
            {
                echo '<p><strong><span id=datecolor>' . htmlspecialchars($donnees['date_message_fr']) . '</span></strong> : <span id=pseudocolor>' . htmlspecialchars($donnees['pseudo']) . '</span> : ' . htmlspecialchars($donnees['message']) . '</p>';
            }

            $reponse->closeCursor();

            ?>
        </div>
      </div>

        <!-- FOOTER -->
        <?php include("../includes/footer.php"); ?>
        
</body>
</html>
