<?php

setcookie('pseudo', $_POST['pseudo'], time() + 365*24*3600, null, null, false, true);


// Je me connecte à la BDD
include("../config.inc.php");

try
{
    $bdd = new PDO("mysql:host=$serveur;dbname=$nomBDD;charset=utf8",$login,$pass);
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}


// J'insère le message avec un requête préparée
$req = $bdd->prepare('INSERT INTO minichat (pseudo, message) VALUES(?, ?)');
$req->execute(array($_POST['pseudo'], $_POST['message']));


// Je redirige vers le minichat
header('Location: minichat.php');

?>