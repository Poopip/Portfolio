<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="portfolio" content="Le site d'Adrien BENION">

    <title>Adrien BENION</title>

  </head>

  <body>

    <!-- MENU PRINCIPAL -->
    <?php include("includes/navigation_principale.php"); ?>

  <div id="main">
    <div class="container">
      <h1 class="text-center">WELCOME</h1>
      <p>Vous vous trouvez sur la page d'accueil de mon site web.</p><br>
      <p>Actuellement en cours de reconversion professionnelle dans le développement Web, ce site a pour vocation de présenter ma progression dans ce domaine. Il est donc perpétuellement en cours d'évolution.</p><br>
      <p>Vous pourrez trouver sur ce site : <br> 
      <ul class="list-group">
        <li class="list-group-item">- Le site en lui-même, conçu avec HTML, CSS, PHP mais aussi avec Bootstrap et partiellement responsive,</li>
        <li class="list-group-item">- un jeu de combat au clic conçu en PHP Objet et MySQL,</li>
        <li class="list-group-item">- un annuaire d'Huiles Essentielles en PHP, MySQL et requêtes AJAX,</li>
        <li class="list-group-item">- un mini-Tchat en PHP également,</li>
        <li class="list-group-item">- et bien d'autres choses à venir...</li>
      </ul>
    </div>
  </div>

    <!-- FOOTER -->
    <?php include("includes/footer.php"); ?>

  </body>
</html>
