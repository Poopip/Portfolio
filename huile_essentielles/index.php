<?php

// ########## Enregistrement autoload

    function chargerClasse($class) 
    {
        require $class . '.php';
    }

    spl_autoload_register('chargerClasse');

    session_start(); // On appelle session_start() après avoir enregistré l'autoload

    if (isset($_GET['deconnexion'])) 
    {
    session_destroy();
    header('Location: .');
    exit();
    }

// ########## Je génère les 26 lettres de l'alphabet pour pouvoir créer l'indexation de l'annuaire

$lettres = array();

for ($i="A"; $i!="AA"; $i++)
{
    $lettres[] = $i;
}

?>

<!DOCTYPE html >
<html > 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Annuaire Huiles Essentielles</title>
<meta name="Robots" content="all"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Requête AJAX-->
<script type="text/javascript"> 

    // Fonction de récupération de l'ID cliqué
    function recupId(that) {
        document.getElementById('tableau').innerHTML = '';
        a = that.id;
        url = 'http://adrienbenion.com/huile_essentielles/liste_annuaire.php?lettre=' + a;
        loadDoc(url);
    };

    // Fonction de traitement de la requête AJAX
    function loadDoc(url) {
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                tableauContent(this.response);
            }
        }
        xhr.open("GET", url, true);
        xhr.send();
    };

    // Fonction de mise à jour du contenu de la div #tableau
    function tableauContent(response) {

        // Je déserialize mon JSON
        let listeExtraite = JSON.parse(response);

        // Je parcours le tableau
        for (i = 0; i < listeExtraite.length; i++) {

            let ligneTableau = document.getElementById('tableau');
            let newLigne = '<tr><td>' + listeExtraite[i].nom + '</td><td>' + listeExtraite[i].nom_latin + '</td><td><a href="#" onClick="confirmation('+ listeExtraite[i].id +');" >Supprimer</a></td><td><a href="modifier.php">modifier</a></td></tr>';

            ligneTableau.innerHTML += newLigne;
        }
    };

    function confirmation(listeExtraite) {
            if (confirm('Etes-vous sur de vouloir supprimer cette huile ?')) {
                alert ("Ok nous allons supprimer !" + listeExtraite);
                location.href='supprimer.php?id=' + listeExtraite;
            }
            else {
                alert ("D'accord je ne fait rien !");
                location.href='index.php';
            }
        };
</script>
</head>

<body>
        
        <!-- MENU PRINCIPAL -->
        <?php include("../includes/navigation_principale.php") ?>

        <!-- DESCRIPTION -->
        <?php include("../includes/descri_he.php") ?>

    <div id="main">
        <h1 class="text-center">Annuaire</h1>

        <!-- FORMULAIRE D'AJOUT -->
        <form action="ajout_he.php" method="post" class="form-row">
                <fieldset>
                    <legend>Ajouter une nouvelle huile essentielle</legend>
                            <div class="col-md-offset-2 col-md-3">
                                <input type="text" id="nom" name="nomCourant" placeholder="Nom" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <input type="text" id="nom_latin" name="nomLatin" placeholder="Nom Latin" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <input type="submit" value="Ajouter" class="form-control">
                            </div>
                </fieldset>
            </form>

        <!-- INDEX DE L'ANNUAIRE-->
        <div id="tabs">
            <ul>
                <?php
                    foreach ($lettres as $lettre) 
                    {
                        echo '<li><a href="#" class="text-center" id="' .$lettre . '" onClick="recupId(this);">' .$lettre . '</a></li>';
                    }
                ?>
            </ul>
        </div>
        <!-- TABLEAU ISSU DE LA REQUETE -->
        <div id="tableauContainer">
            <table id="tableau">
            </table>
        </div>
    </div>
        <!-- FOOTER -->
        <?php include("../includes/footer.php"); ?>

</body>
</html>