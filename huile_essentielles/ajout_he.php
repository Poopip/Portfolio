<?php

    // CONNEXION BDD
    include("../config.inc.php");

    // Préparation de la requête d'insertion
        $insert = $db->prepare('INSERT INTO liste_he VALUES (NULL, :nom, :nom_latin)');


    // On lie chaque marqueur à une valeur
        $insert->bindValue(':nom', $_POST['nomCourant'], PDO::PARAM_STR);
        $insert->bindValue(':nom_latin', $_POST['nomLatin'], PDO::PARAM_STR);

    // Execution de la requête préparée
        $insertIsOk = $insert->execute();

        if ($insertIsOk) {
            $message = 'La nouvelle huile essentielle a bien été ajoutée';
        }
        else {
            $message = 'Echec de l\'insertion';
        }

    echo $message;

?>

<button class="form-control"><a href="index.php" >Retour</a></button>