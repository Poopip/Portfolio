<?php 

class heManager
{
    // CONNEXION BDD
    private $db; // Instance de PDO

    public function __construct($db)
    {
        $this->db = $db;
    }

    // DECLARATION DES FONCTIONNALITES
    public function add(He $he)
    {
        $q = $this->db->prepare('INSERT INTO liste_he(nom, nom_latin) VALUES(:nom, :nom_latin)');
        $q->bindValue(':nom', $he->nom());
        $q->bindValue(':nom_latin', $he->nom_latin());
        $q->execute();

        $he->hydrate([
            'id' => $this->db->lastInsertId(),
            //'nom' => ,// récupérer le nom dans le champ rempli 
            //'nom_latin' => // récupérer le nom dans le champ rempli
        ]);
    }

    public function delete(He $he)
    {
        $this->db->exec('DELETE FROM liste_he WHERE id = '.$he->id());
    }

    public function get($info)
    {
        $he = [];
        if (is_int($info))
        {
            $q = $this->db->query('SELECT id, nom, nom_latin FROM liste_he WHERE id = '.$info);
            $he = $q->fetch(PDO::FETCH_ASSOC);
            
            return new He($he);
        }
        else
        {
            $q = $this->db->prepare('SELECT id, nom, nom_latin FROM lsite_he WHERE nom = :nom');
            $q->execute([':nom' => $info]);

            return new He($q->fetch(PDO::FETCH_ASSOC));
        }
    }

    public function getList($nom)
    {
        $retourHes = [];
        $q = $this->db->prepare('SELECT id, nom, nom_latin FROM liste_he WHERE nom <> :nom ORDER BY nom'); 
        // Voir modifier la requête de nom <> :nom pour les initiales ?
        $q->execute([':nom' => $nom]);

        while ($donnees = $q->fetch(PDO::FETCH_ASSOC))
        {
            $retourHes[] = new He($donnees);
        }
        
        return $retourHes;
        
    }

    public function setDb(PDO $db)
    {
        $this->db = $db;
    }    
}

// Fonctions a prévoir :
    // Vérifier que le nom de l'HE n'existe pas déjà avec MySQL probablement et/ou une fonction exist()



?>
