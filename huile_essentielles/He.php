<?php 

/////////////////////////////////////////////
// DEBUT CLASSE Huile Essentielle

class He
{
     // ATTRIBUTS
    protected $id, 
              $nom,
              $nom_latin;

    // CONSTRUCTEUR
    public function __construct(array $donnees)
    {
        $this->hydrate($donnees);
    }

    // FONCTIONNALITES

    // HYDRATATION
    public function hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value)
        {
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method))
            {
                $this->$method($value);
            }
        }
    }

    // GETTERS (Lire les attributs)
    public function id()
    {
        return $this->id;
    }

    public function nom()
    {
        return $this->nom;
    }

    public function nom_latin()
    {
        return $this->nom_latin;
    }

    // SETTERS (Modifier les valeurs des attributs)
    public function setId($id)
    {
        $id = (int) $id;
        if ($id > 0)
        {
            $this->id = $id;
        }
    }

    public function setNom($nom)
    {
        if (is_string($nom))
        {
            $this->nom = $nom;
        }
    }

    public function setNom_Latin($nom_latin)
    {
        if (is_string($nom_latin))
        {
            $this->nom_latin = $nom_latin;
        }
    }
}

// FIN CLASSE Huile Essentielle
/////////////////////////////////////////////

?>