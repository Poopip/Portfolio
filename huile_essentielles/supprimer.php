<?php

    // CONNEXION BDD
    include("../config.inc.php");

    // ########## REQUETE DE SUPPRESSION DANS LA BDD ##########

        // Préparation de la requête
        $supp_extrait = ' DELETE 
        FROM liste_he
        WHERE id=:num LIMIT 1';

        $supprimer = $db->prepare($supp_extrait);

        // Liaison du paramètre nommé
        $supprimer->bindValue(':num', $_GET['id'], PDO::PARAM_INT);

        // Execution de la requete préparée
        $supprimerIsOk = $supprimer->execute();

        if ($supprimerIsOk) {
            $message = 'L\'huile essentielle a bien été supprimée';
        }
        else {
            $message = 'Echec de la suppression';
        }

        echo $message

?>

    <button class="form-control"><a href="index.php" >Retour</a></button>